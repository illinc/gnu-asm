#ifdef __APPLE__
#define FNAME(s) _##s
#else
#define FNAME(s) s
#endif 

.data 
    fmt: .string " A=%016llX,  C=%016llX,   D=%016llX,   B=%016llX, si=%016llX, di=%016llX,\nr8=%016llX, r9=%016llX, r10=%016llX, r11=%016llX\n" 
   
.text
.globl FNAME(main)

FNAME(main):        
    push %rbx 
    // 11 [fmt, A, C, D, B, di, si, r8, r9, r10, r11) - 6 [di, si, D, C, r8, r9] = 5
    #define STACK_PARAMS_SIZE 8*6
    sub $STACK_PARAMS_SIZE, %rsp 
        
    // SysV amd64: di, si, D, C, r8, r9 + al
    mov %rdi,   (%rsp)
    mov %r8,   8(%rsp)
    mov %r9,  16(%rsp)
    mov %r10, 24(%rsp)
    mov %r11, 32(%rsp)
    mov %rsi, %r9
    
    lea fmt(%rip), %rdi
    mov %rax, %rsi
    xchg %rcx, %rdx
    mov %rbx, %r8

    xor %eax, %eax  
    call FNAME(printf)

    add $STACK_PARAMS_SIZE, %rsp 
    #undef STACK_PARAMS_SIZE
    
    pop %rbx
    xor %eax, %eax 
    ret 
