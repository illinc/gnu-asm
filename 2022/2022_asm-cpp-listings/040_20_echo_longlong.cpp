// Расширенный вариант эха, с приглашением и контролем

#include <stdio.h>

int main()
{
    long long x = 0;
    
    puts("Введите целое число: ");
    
    int result = scanf("%lld", &x);
    
    if (1 == result)
        printf("%lld\n", x);
    else
        puts("не число!");
    
    return 0;
}
