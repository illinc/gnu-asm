#include <cstdlib>
#include <cstdio>
#include <cstring>

#define PRINT_AND_PERFORM(x) puts(""); puts(#x); x

#define PRINT_FLAG(tag, r, b) printf("%s[%2d] %9s: %X\n", #r, b, #tag, ( r & (1<<b) ) !=0 );

void cpuid_test(unsigned int *pA, unsigned int *pB, unsigned int *pC, unsigned int *pD)
{
    asm(
    "cpuid\n"
    : "+a"(*pA), "+b"(*pB), "+c"(*pC), "+d"(*pD) 
    );    

    printf("%08c %08c %08c %08c\n", 'A', 'B', 'C', 'D');
    printf("%08X %08X %08X %08X\n", *pA, *pB, *pC, *pD);
}

int main() 
{
    unsigned int A = -1, B = -1, C = -1, D = -1;
    
    puts("*** CPUID ***");
    puts("\nVendor");
    PRINT_AND_PERFORM(A = 0;)
    cpuid_test(&A, &B, &C, &D);
    
    char s[3*4+1]; 
    
    //strncpy(s,   reinterpret_cast<const char*>(&B), 4);
    //strncpy(s+4, reinterpret_cast<const char*>(&D), 4);
    //strncpy(s+8, reinterpret_cast<const char*>(&C), 4);
    
    *(reinterpret_cast<unsigned int*>(s)) = B;
    *(reinterpret_cast<unsigned int*>(s)+1) = D;
    *(reinterpret_cast<unsigned int*>(s)+2) = C;
  
    s[3*4] = 0; 
    printf("%s\n", s);
    
    
    // AMD
    // D.2 CPUID Feature Flags Related to Instruction Support
    // Table D-1. Feature Flags for Instruction / Instruction Subset Support
    // сводная таблица про расширения и CPUID

    // 40332 Rev 4.02 November 2020 AMD64 Architecture Programmer's Manual, Volumes 1-5
    // в разделе команды VSUBPD:  VSUBPD  AVX CPUID Fn0000_0001_ECX[AVX] (bit 28)
    // описания отдельных команд
    
    
    // Intel
    // INSTRUCTION SET REFERENCE, A-L
    // в разделе команды CPUID
    // Table 3-8. Information Returned by CPUID Instruction
    // Initial EAX Value 01H
    // EAX Version Information: Type, Family, Model, and Stepping ID (see Figure 3-6).
    // EBX
    // Bits 07 - 00: Brand Index.
    // Bits 15 - 08: CLFLUSH line size (Value ∗ 8 = cache line size in bytes; used also by CLFLUSHOPT).
    // Bits 23 - 16: Maximum number of addressable IDs for logical processors in this physical package*.
    // Bits 31 - 24: Initial APIC ID.
    // ECX Feature Information (see Figure 3-7 and Table 3-10).
    // EDX Feature Information (see Figure 3-8 and Table 3-11).
    // NOTES:
    // * The nearest power-of-2 integer that is not smaller than EBX[23:16] is the number of unique initial APIC
    // IDs reserved for addressing different logical processors in a physical package. This field is only valid if
    // CPUID.1.EDX.HTT[bit 28]= 1.
    
    // Table 3-10. Feature Information Returned in the ECX Register
    // 28 AVX A value of 1 indicates the processor supports the AVX instruction extensions.
    
    
    // номера битов регистров результата CPUID и у AMD, и у Intel — с нуля    
    
    puts("\nVersion & features");
        
    PRINT_AND_PERFORM(A = 1;)
    cpuid_test(&A, &B, &C, &D);    
    
    PRINT_FLAG(FPU,    D, 0)
    PRINT_FLAG(sysenter/sysexit, D, 11)
    PRINT_FLAG(cmovCC, D, 15)
    
    PRINT_FLAG(MMX Extensions,  D, 22)
    PRINT_FLAG(MMX,  D, 23)
    PRINT_FLAG(fxsave/fxrstor,  D, 24)
    PRINT_FLAG(SSE,  D, 25)
    PRINT_FLAG(SSE2, D, 26)
     
    PRINT_FLAG(Long Mode, D, 29)
   
    PRINT_FLAG(3DNow! Extensions,  D, 30)
    PRINT_FLAG(3DNow!,  D, 31)
    
    
    PRINT_FLAG(SSE3,  C, 0)
    PRINT_FLAG(AMD SSE4A,  C, 6)
    PRINT_FLAG(SSSE3, C, 9)
   
    PRINT_FLAG(SSE4.1, C, 19)
    PRINT_FLAG(SSE4.3, C, 20)
    PRINT_FLAG(AES, C, 25)
    PRINT_FLAG(AVX, C, 28)


    PRINT_AND_PERFORM(A = 0x80000001;)
    cpuid_test(&A, &B, &C, &D);    
    PRINT_FLAG(lahf/sahf, C, 0)
    PRINT_FLAG(AMD XOP, C, 11)
    PRINT_FLAG(syscall/sysret, D, 11)
   
    
    PRINT_AND_PERFORM(A = 7; C = 0;)
    cpuid_test(&A, &B, &C, &D);    
    PRINT_FLAG(AVX2, B, 5)
    
  
   
    
    return 0;
}
