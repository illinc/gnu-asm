#define M_PI 3.14159265358979323846  /* pi */  // украдено из math.h,  #include <math.h> нельзя: там не только #define, но и заголовки функций 

.data
    fmt_out: .string "sin(%lg) = %lg\n"
    static_Pi: .double M_PI             // M_PI/6 нельзя, компилятор рассчитывает только целочисленные выражения
    
.text
.globl main


// Используем макросы, чтобы при изменении числа и расположения локальных переменных менять одну строку, а не несколько в разных местах

.func
main:

    #define STACK_ADJUST_SIZE 8*3
    sub $STACK_ADJUST_SIZE, %rsp        
    #define LOCAL_X_ADDR     (%rsp)
    #define LOCAL_SINX_ADDR 8(%rsp)
    
    // x = pi/6 → LOCAL_X_ADDR
    vmovsd static_Pi(%rip), %xmm0
    mov $6, %r8d
    vcvtsi2sd %r8d, %xmm1, %xmm1 // преобразование long %r8d → double %xmm1, старшую часть %xmm1 берём из %xmm1 же
    vdivsd %xmm1, %xmm0, %xmm0 // xmm0/xmm1 → xmm0
    vmovsd %xmm0, LOCAL_X_ADDR
    
    // sin(x) → LOCAL_SINX_ADDR
    vmovsd LOCAL_X_ADDR, %xmm0
    call sin
    vmovsd %xmm0, LOCAL_SINX_ADDR
          
    // printf("sin(%lg) = %lg\n", x, sin(x));
    lea fmt_out(%rip), %rdi
    vmovsd LOCAL_X_ADDR,    %xmm0
    vmovsd LOCAL_SINX_ADDR, %xmm1
    mov $2, %al   
    call printf
       
    xor %eax, %eax
    add $STACK_ADJUST_SIZE, %rsp
    #undef STACK_ADJUST_SIZE
    #undef LOCAL_X_ADDR
    #undef LOCAL_SINX_ADDR
    
    ret 
.endfunc
