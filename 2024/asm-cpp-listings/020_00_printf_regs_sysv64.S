// www.onlinegdb.com        ← корёжит русские комментарии под MS Windows, GNU/Linux — нормально
// https://www.jdoodle.com/compile-assembler-gcc-online/    ← не понимает комментарии после мнемоники — ни //, ни ; (надо отдельной строкой); есть stdin ← 2024, GNU/Linux — понимает

.data 
    fmt: .string " A=%016llX,  C=%016llX,   D=%016llX,   B=%016llX, si=%016llX, di=%016llX,\n\
r8=%016llX, r9=%016llX, r10=%016llX, r11=%016llX\n\
xmm0[0] = %5lg, xmm1[0] = %5lg, xmm2[0] = %5lg, xmm3[0] = %5lg, xmm4[0] = %5lg,\n\
xmm5[0] = %5lg, xmm6[0] = %5lg, xmm7[0] = %5lg, xmm8[0] = %5lg, xmm9[0] = %5lg"
    
    a: .quad 0xFFFFFFFF0000000A    
    x: .double 3.11
    
    arr: .long 0xAA, 0xAB, 0xAC, 0xAD, 0xAE
    arr_end:
   
.text
.globl main

main:        
    push %rbx 
    // Ц/ч: 11 [fmt, A, C, D, B, si, di, r8, r9, r10, r11] - 6 [di, si, D, C, r8, r9] = 5; xmm: 10 [xmm0-xmm9] - 8 [xmm0-xmm7] = 2
    // итого в~стеке 7 параметров (для выравнивания → 8) по 8 байт
    sub $(8*8), %rsp 
    
    // ************************************************************************  
    // Разные действия с регистрами
    
    // A = xmm0 = 9
    mov $9, %rax    
    vcvtsi2sd %rax, %xmm0, %xmm0
    
    // C = xmm1 = 3.11
    vmovsd x(%rip), %xmm1
    vcvtsd2si %xmm1, %rcx
    
    // D = A - C = 9-3 = 6
    mov %rax, %rdx
    sub %rcx, %rdx
    
    // xmm2 = xmm0-xmm1 = 9.00 - 3.11 = 5.89    
    vsubsd %xmm1, %xmm0, %xmm2
     
    // xmm3 = static_cast<double>(a как 32 или 64 бита)  
    // lo(A) = 10 → xmm3,   A = -2^32+10 ≈ −4.2⋅10^9 → xmm4
    vcvtsi2sdl a(%rip), %xmm3, %xmm3 
    vcvtsi2sdq a(%rip), %xmm4, %xmm4 
        
    vcvtsi2sd  a(%rip), %xmm5, %xmm5 
    // ↑↑↑ получаем xmm5=xmm3, то есть без суффикса (по умолчанию) l
    
   
    // arr[2] → B
    lea arr(%rip), %rbx
    mov $2, %rsi
    mov (%rbx, %rsi, 4), %ebx 
    
    
    mov $((arr_end-arr)/4), %rsi
    mov $-1, %r8d
   
    vmovsd %xmm0, %xmm0, %xmm8
    
    // di, r9, r10, r11, xmm6, xmm7, xmm9 не изменялись

    
    // ************************************************************************  
    // printf(fmt, A, C, D, B, si, di, r8, r9, r10, r11)
    // SysV amd64: di, si, D, C, r8, r9, xmm0-xmm7 + al
    
    // в стек 5 последних целочисленных параметров: di, r8, r9, r10, r11 и 2 xmm:
    mov %rdi,   (%rsp)
    mov %r8,   8(%rsp)
    mov %r9,  16(%rsp)
    mov %r10, 24(%rsp)
    mov %r11, 32(%rsp)
    
    vmovsd %xmm8, 40(%rsp)
    vmovsd %xmm9, 48(%rsp)
    
    // в регистры 6 первых целочисленных; 8 первых xmm уже в xmm0-xmm7
    // [fmt, A, C, D, B, si] → [di, si, D, C, r8, r9]
    lea fmt(%rip), %rdi
    mov %rsi, %r9    
    mov %rax, %rsi 
    xchg %rcx, %rdx
    mov %rbx, %r8

    mov $8, %al 
    call printf

    add $(8*8), %rsp 

    pop %rbx
    xor %eax, %eax 
    ret 
