# noexec на флешке #!/bin/bash

MAINFILE=''
SRCDIRNAME=''
NOEXTBASEFILENAME=''

SRCROOTDIR='tex-src'

TARGETTYPE=''

RUNSCOUNT='1'
HELPLINE="Запуск: bash ${0} [ b|book|p N|pres N|l|buglist -1|-2|-f|--full | -h]"

numargs="$#"
for ((i=1 ; i <= numargs ; i++)); do

  arg="${1}"
  shift
  
  case "$arg" in
  -1)    
    RUNSCOUNT='1'
    ;;
  -2)    
    RUNSCOUNT='2'
    ;;     
  -f|--full)    
    RUNSCOUNT='f'
    ;; 
#   --bib)    
#     RUNSCOUNT='bib'
#     ;;    
  b|book)    
    SRCDIRNAME="book"
    TARGETTYPE="book"
    NOEXTBASEFILENAME=0main
    ;;     
  bx|nextbook)    
    SRCDIRNAME="book-2025"
    TARGETTYPE="book"
    NOEXTBASEFILENAME=0main
    ;;     
  l|buglist)    
    SRCDIRNAME="book"
    NOEXTBASEFILENAME=0bugs
    ;;     
  p|pres)    
    SRCDIRNAME="pres"
    FILENAMEWITHPATH=`ls ${SRCROOTDIR}/${SRCDIRNAME}/${1}*`
    TEXBASEFILENAME=`basename ${FILENAMEWITHPATH}`
    NOEXTBASEFILENAME="${TEXBASEFILENAME/.tex/}"
    echo $NOEXTBASEFILENAME
    shift
    ;;
  d)    
    MAINFILE="${1}"
    shift
    
    TEXBASEFILENAME=`basename ${MAINFILE}`
    NOEXTBASEFILENAME="${TEXBASEFILENAME/.tex/}"
    RESFILE=${MAINFILE/.tex/.pdf}
    ;;
  i)    
    SRCDIRNAME="tex2img" # это тоже анализируется, и при «» — перезаписывается RESFILE!
    MAINFILE="${1}"
    shift
    
    TEXBASEFILENAME=`basename ${MAINFILE}`
    NOEXTBASEFILENAME="${TEXBASEFILENAME/.tex/}"
    RESFILE="${SRCROOTDIR}/img/"${TEXBASEFILENAME/.tex/.pdf}""
    echo "${MAINFILE}"
    echo "${TEXBASEFILENAME}"
    echo "${RESFILE}"
    #exit
    ;;
       
  -h)
    echo "${HELPLINE}"
    ;;    
  esac  
done



if [[ (-z "${NOEXTBASEFILENAME}") ]]
then
  echo "Не задано имя файла."
  echo "${HELPLINE}"
  exit
fi


if ! [ -d tmp/ ]; then
  mkdir tmp/
fi


if [[ (-z "${MAINFILE}") ]]
then

    MAINFILE=${SRCROOTDIR}/${SRCDIRNAME}/${NOEXTBASEFILENAME}
fi    

COMPILEDFILE="tmp/${NOEXTBASEFILENAME}.pdf"

# COMPRESSEDFILE="${COMPILEDFILE}.pdf"
MINSIZEFILE=${COMPILEDFILE}

# RESFILE="kai-rndnet-${NOEXTBASEFILENAME}.pdf"




MKTXT="pdflatex --output-directory=tmp ${MAINFILE}"
MKBIB="bibtex tmp/${NOEXTBASEFILENAME}"

echo "${MKTXT}"

# pdflatex ${NOEXTBASEFILENAME}

# case "$SRCDIRNAME" in
case "$TARGETTYPE" in
  book)
  RESFILE=gnu-asm-theory-labs.pdf
  
if [[ "${NOEXTBASEFILENAME}" == "0bugs" ]]; then
  RESFILE=Замеченные\ опечатки.pdf
fi  
  

  case "$RUNSCOUNT" in
    1)    
      ${MKTXT}
    ;;     
    2)    
      ${MKTXT} &&  ${MKTXT}
    ;;     
#     bib)    
# #         ${MKTXT} && ${MKTXT} && ${MKBIB} && ${MKBIB} && ${MKTXT}&& ${MKTXT}
# 	rm "tmp/${NOEXTBASEFILENAME}.bbl" "tmp/${NOEXTBASEFILENAME}.blg"
#         ${MKBIB} && ${MKBIB} && ${MKTXT} # удалять только bib
#     ;;      
    f)        
#       ${MKTXT} &&  ${MKTXT} && ${MKBIB} && ${MKBIB} && ${MKTXT} &&  ${MKTXT}
      if ${MKTXT} 
      then
      
        ${MKTXT} && ${MKBIB} && ${MKBIB} && ${MKTXT}
        
        cd tmp/
        # utf-8 не годится, нужна однобайтовая кодировка (любая подходит, но официально результат rumakeindex в koi8)
        LANG=ru_RU.KOI8-RU rumakeindex ${NOEXTBASEFILENAME}.idx
        iconv -f KOI8-RU -t WINDOWS-1251 ${NOEXTBASEFILENAME}.ind -o ${NOEXTBASEFILENAME}.ind
        cd ..
        
        ${MKTXT} && ${MKTXT}
  
  
        # ps2pdf -dUseFlatCompression=true ${COMPILEDFILE}
      else
	exit
      fi
    ;;    
  esac  
  
  ;;
  

  
  pres|'')
  RESFILE=${NOEXTBASEFILENAME}.pdf
  ;&
  
  *)
  case "$RUNSCOUNT" in
    1)    
      ${MKTXT}
    ;;     
    2|f)    
      ${MKTXT} &&  ${MKTXT}
    ;;    
  esac  

  
esac



if [[ "${SRCDIRNAME}" == "tex2img" ]]; then
    pdfcrop --margins=0  "${MINSIZEFILE}" "${MINSIZEFILE}"
fi 

echo "Копирую ${MINSIZEFILE} в ${RESFILE}..."
cp "${MINSIZEFILE}" "${RESFILE}"

# echo "Копирую в корень флешки..."
# cp ${MINSIZEFILE} "../../${RESFILE}"

# echo "Копирую в github-stub..."
# cp ${MINSIZEFILE} "../github-stub/${RESFILE}"

