#ifdef __APPLE__
#define FNAME(s) _##s
#elif _WIN64
#define FNAME(s) s
#elif _WIN32
#define FNAME(s) _##s
#else
#define FNAME(s) s
#endif 

.data
    fmt: .string "%X=%s\n"
    tag: .string "AVX"
    
.global FNAME(main) 

.text
// int main()
FNAME(main):
    push %rbx // 1) выравнивание rsp для printf(); 2) cpuid меняет ebx

    //  asm("cpuid\n" : "=c"(C) : "a"(1) : "ebx", "edx");  
    mov $1, %eax
    cpuid
    
    //  bool AVX_bit = ( C&(1<<28) ) !=0;
    xor %esi, %esi
    test $(1<<28), %ecx // ZF =  [ C&(1<<28) ) ==0 ]
    setnz %sil    
    
    //  printf("%X=%s\n", AVX_bit, "AVX"); ← SysV amd64: di, si, D, C, r8, r9 + al
    lea fmt(%rip), %rdi
    // AVX_bit уже в %esi
    lea tag(%rip), %rdx
    mov $0, %al   
    call FNAME(printf)
        
    pop %rbx
    
    // return 0;
    xor %eax, %eax 
    ret 
