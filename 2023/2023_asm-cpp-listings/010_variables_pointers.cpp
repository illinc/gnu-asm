// https://www.onlinegdb.com/

#include <iostream>

using namespace std;

int g1 = 0;    
int g2; 

const int N = 5;

// n!    
int fact(int n)
{
    int f; 
    if (n <= 2)  f = n;  else  f = n * fact(n-1);
    return f;
}

int main(){
    int i1 = -1; 
    static int i2 = 0; 
    printf("%d %d;\t", i1, i2);
    
    int *p1 = &g1; 
    int *p2 = &i1;
    int *p3 = new int; 
    int *p4 = new int [N];
    *p2 = fact(3);

    printf("%d %d;\t", i1, i2);

    delete p3;
    delete [] p4;
    return 0;
}
