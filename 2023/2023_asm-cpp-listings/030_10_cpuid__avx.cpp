#include <cstdlib>
#include <cstdio>

int main() 
{
    unsigned int C;    
    asm(
        "cpuid\n"
        : "=c"(C)
        : "a"(1)
        : "ebx", "edx"
    );  
    bool AVX_bit = ( C&(1<<28) ) !=0;
    
    printf("%X=%s\n", AVX_bit, "AVX");
        
    return 0;
}
